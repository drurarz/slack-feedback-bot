package eu.rurarz.slack.bot.feedback;

import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.EventType;
import me.ramswaroop.jbot.core.slack.SlackService;
import me.ramswaroop.jbot.core.slack.models.Event;
import me.ramswaroop.jbot.core.slack.models.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.regex.Matcher;

/**
 * Feedback bot for Slack.
 * 
 * @author Daniel Rurarz
 * @version 0.5 9/12/2017
 */
@Component
public class SlackBot extends Bot {

    private static final Logger logger = LoggerFactory.getLogger(SlackBot.class);

    /**
     * Slack token from application.properties file. 
     */
    @Value("${slackBotToken}")
    private String slackToken;
    
    
    // TODO - it is not multi-user at all :-)
    private String who;
    private String message;

    @Override
    public String getSlackToken() {
        return slackToken;
    }

    @Override
    public Bot getSlackBot() {
        return this;
    }

    @Controller(events = {EventType.DIRECT_MESSAGE})
    public void onReceiveDM(WebSocketSession session, Event event) {
        reply(session, event, new Message("Hi, if you want to send feedback, just type 'feedback' and I will guide you."));
    }
    
    @Controller(events = {EventType.DIRECT_MENTION})
    public void onReceiveMention(WebSocketSession session, Event event) {
        reply(session, event, new Message("Hi, if you want to send feedback, just start a direct conversation with me, type 'feedback' and I will guide you."));
    }

   
    @Controller(events = {EventType.DIRECT_MESSAGE}, pattern = "(feedback)", next = "askForTheMessage")
    public void createFeedback(WebSocketSession session, Event event) {
        startConversation(event, "askForTheMessage");   // start conversation
        reply(session, event, new Message("Cool! I can send any anonymous you want. Who should receive it?"));
    }
    
    @Controller(events = {EventType.DIRECT_MESSAGE}, next = "confirmFeedback")
    public void askForTheMessage(WebSocketSession session, Event event) {
    	
    	// TODO replace this strange ID with a proper name
    	who = event.getText();
    	
    	reply(session, event, new Message("I will send you message to " + event.getText() +
            ". What do you want me to deliver?"));
    	nextConversation(event);    // jump to next question in conversation
    }
    
    @Controller(events = {EventType.DIRECT_MESSAGE}, next = "finishFeedbackCollection")
    public void confirmFeedback(WebSocketSession session, Event event) {
    	message = event.getText();
    	reply(session, event, new Message("Your message to " + who + " is '" + event.getText() +
            "'. Are you sure?"));
    	
    	nextConversation(event);    // jump to next question in conversation
    }
    
    @Controller(events = {EventType.DIRECT_MESSAGE})
    public void finishFeedbackCollection(WebSocketSession session, Event event) {
        if (event.getText().contains("yes")) {
            reply(session, event, new Message("Great! I am sending an anonymous message '" + message + "' to " + who + "."));
            
            // TODO: send it to the receiver
        } else {
            reply(session, event, new Message("Sure, maybe next time."));
        }
        stopConversation(event);    // stop conversation
    }
}